/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.yougotagift.eventHandler;

import javax.enterprise.event.Observes;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.client.kernel.event.EntityUpdateEvent;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;

import com.openbravo.sharaf.integration.retail.yougotagift.CustyggWebserviceConfig;

public class CustyggEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      CustyggWebserviceConfig.ENTITY_NAME) };
  protected Logger logger = Logger.getLogger(this.getClass());

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onUpdate(@Observes EntityUpdateEvent event) {
    validateData(event);
  }

  public void onSave(@Observes EntityNewEvent event) {
    validateData(event);
  }

  private void validateData(EntityPersistenceEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    final Entity transactionEntity = ModelProvider.getInstance().getEntity(
        CustyggWebserviceConfig.ENTITY_NAME);
    final Boolean active = (Boolean) event.getCurrentState(transactionEntity
        .getProperty(CustyggWebserviceConfig.PROPERTY_ACTIVE));
    // Check if Method is POST
    final String checkMethod = (String) event.getCurrentState(transactionEntity
        .getProperty(CustyggWebserviceConfig.PROPERTY_CHECKHTTPMETHOD));
    if (!checkMethod.equals("POST")) {
      throw new OBException("@CUSTYGG_InvalidHttpMethod@");
    }
    final String reedemMethod = (String) event.getCurrentState(transactionEntity
        .getProperty(CustyggWebserviceConfig.PROPERTY_REDEEMHTTPMETHOD));
    if (!reedemMethod.equals("POST")) {
      throw new OBException("@CUSTYGG_InvalidHttpMethod@");
    }
    // Check if there is more than one active configuration
    if (active) {
      final String id = (String) event.getId();
      OBCriteria<CustyggWebserviceConfig> criteria = OBDal.getInstance().createCriteria(
          CustyggWebserviceConfig.class);
      criteria.add(Restrictions.eq(CustyggWebserviceConfig.PROPERTY_ACTIVE, true));
      criteria.add(Restrictions.ne(CustyggWebserviceConfig.PROPERTY_ID, id));
      criteria.setMaxResults(1);
      if (criteria.uniqueResult() != null) {
        throw new OBException("@CUSTYGG_AlreadyDefined@");
      }
    }
  }
}
