/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.yougotagift.process;

import java.io.IOException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.openbravo.sharaf.integration.retail.yougotagift.CUSTYGGUtils;
import com.openbravo.sharaf.integration.retail.yougotagift.CustyggWebserviceConfig;

public class CallCheckaGiftCard extends JSONProcessSimple {
  private static final Logger log = LoggerFactory.getLogger(CallCheckaGiftCard.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) {
    try {
      log.debug("Start YGAG request: {}", jsonsent.toString(2));
    } catch (JSONException ignore) {
    }
    JSONObject parameters = jsonsent.optJSONObject("parameters");

    JSONObject response = new JSONObject();
    try {
      OBContext.setAdminMode(true);
      CustyggWebserviceConfig custyggConfig = CUSTYGGUtils.getCUSTYGGConfig();
      JSONObject json = CUSTYGGUtils.prepareCheckaGiftCardPOSTJsonInfo(parameters.optJSONObject(
          "yggCardNumber").optString("value"));
      response = CUSTYGGUtils.sendCheckGiftCardPetition(custyggConfig, json);
    } catch (OBException | IOException e) {
      log.error("Unexpected error processing call", e);
      try {
        response.put("myError", e);
      } catch (JSONException ignore) {
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    JSONObject result = new JSONObject();
    try {
      result.put(JsonConstants.RESPONSE_DATA, response);
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (JSONException ignoree) {
    }
    return result;
  }
}