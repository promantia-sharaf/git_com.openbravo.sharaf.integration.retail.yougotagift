/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.integration.retail.yougotagift.hooks;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.OBDateUtils;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.retail.posterminal.OrderLoaderPreProcessPaymentHook;

@ApplicationScoped
public class CustyggOrderLoaderHook extends OrderLoaderPreProcessPaymentHook {
  public void exec(JSONObject jsonorder, Order order, JSONObject jsonpayment, FIN_Payment payment)
      throws Exception {

    if (jsonpayment.has("custyggCode")) {
      String yggCode = jsonpayment.getString("custyggCode");
      payment.setCustyggCode(yggCode);
      String date = jsonpayment.getString("custyggDateRedeemed").toString();
      payment.setCustyggDateRedeemed(OBDateUtils.getDate(date.replaceAll("/", "-")));
    }
    if (jsonpayment.has("custyggStaff")) {
      String yggStaff = jsonpayment.getString("custyggStaff");
      payment.setCustyggStaff(yggStaff);
    }
    if (jsonpayment.has("custyggBrand")) {
      String yggBrand = jsonpayment.getString("custyggBrand");
      payment.setCustyggBrand(yggBrand);
    }
    if (jsonpayment.has("custyggRedemptionCode")) {
      String yggRedemptionCode = jsonpayment.getString("custyggRedemptionCode");
      payment.setCustyggRedemptionCode(yggRedemptionCode);
    }
    if (jsonpayment.has("custyggStore")) {
      String yggStore = jsonpayment.getString("custyggStore");
      payment.setCustyggStore(yggStore);
    }
    if (jsonpayment.has("custyggCardAmount")) {
      String yggCardAmount = jsonpayment.getString("custyggCardAmount");
      payment.setCustyggCardAmount(new BigDecimal(yggCardAmount));
    }
  }
}
