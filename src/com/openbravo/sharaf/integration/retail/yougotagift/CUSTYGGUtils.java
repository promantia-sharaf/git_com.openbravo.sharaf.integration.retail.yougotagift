/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.integration.retail.yougotagift;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.utils.FormatUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CUSTYGGUtils {
  private static final Logger log = LoggerFactory.getLogger(CUSTYGGUtils.class);

  private static HttpPost preparePostPetition(String request, JSONObject bodyContent)
      throws UnsupportedEncodingException {
    HttpPost post = new HttpPost(request);
    post.setHeader("Content-type", "application/json");
    if (!(bodyContent == JSONObject.NULL || bodyContent == null)) {
      List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
      urlParameters.add(new BasicNameValuePair("code", bodyContent.optString("code")));
      post.setEntity(new UrlEncodedFormEntity(urlParameters));
    }
    return post;
  }

  public static HttpPost prepareRedeemPostPetition(String request, JSONObject bodyContent)
      throws UnsupportedEncodingException {
    HttpPost post = new HttpPost(request);
    post.setHeader("Content-type", "application/json");
    if (!(bodyContent == JSONObject.NULL || bodyContent == null)) {
      List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
      urlParameters.add(new BasicNameValuePair("code", bodyContent.optString("code")));
      urlParameters.add(new BasicNameValuePair("store_staff_position", bodyContent
          .optString("store_staff_position")));
      urlParameters.add(new BasicNameValuePair("store_staff_name", bodyContent
          .optString("store_staff_name")));
      urlParameters.add(new BasicNameValuePair("store_location", bodyContent
          .optString("store_location")));
      post.setEntity(new UrlEncodedFormEntity(urlParameters));
    }
    return post;
  }

  // Check a Gift Card

  public static JSONObject sendCheckGiftCardPetition(CustyggWebserviceConfig custyggConfig,
      JSONObject bodyContent) throws ClientProtocolException, IOException {
    try {
      log.debug("Request content: {}", bodyContent.toString(2));
    } catch (JSONException ignore) {
    }
    HttpClient client = createClient(custyggConfig);
    HttpResponse httpResponse = null;
    HttpPost post = preparePostPetition(custyggConfig.getCheckAGiftCardPath(), bodyContent);
    httpResponse = client.execute(post);

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponse
        .getEntity().getContent()));
    StringBuilder content = new StringBuilder();
    String line = "";
    while ((line = bufferedReader.readLine()) != null) {
      content.append(line);
    }
    log.debug("Received response: {}", content);
    try {
      return new JSONObject(content.toString());
    } catch (JSONException e) {
      throw new OBException("Returned string is not a valid json", e, true);
    }
  }

  // Gift Card Redemption

  public static JSONObject sendGiftCardRedemptionPetition(CustyggWebserviceConfig custyggConfig,
      JSONObject bodyContent) throws ClientProtocolException, IOException, OBException {
    try {
      log.debug("Request content: {}", bodyContent.toString(2));
    } catch (JSONException ignore) {
    }
    HttpClient client = createClient(custyggConfig);
    HttpResponse httpResponse = null;
    HttpPost post = prepareRedeemPostPetition(custyggConfig.getRedeemAGiftCardPath(), bodyContent);
    httpResponse = client.execute(post);

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpResponse
        .getEntity().getContent()));
    StringBuilder content = new StringBuilder();
    String line = "";
    while ((line = bufferedReader.readLine()) != null) {
      content.append(line);
    }
    log.debug("Received response: {}", content);
    try {
      return new JSONObject(content.toString());
    } catch (JSONException e) {
      throw new OBException("Returned string is not a valid json", e, true);
    }
  }

  // Create HttpClient

  private static HttpClient createClient(CustyggWebserviceConfig custyggConfig) {
    if (custyggConfig.getCustyggUser() == null || custyggConfig.getCustyggPassword() == null) {
      throw new OBException(OBMessageUtils.messageBD("CUSTYGG_ErrorUserPass"));
    }
    String passwordDecrypted = decryptPassword(custyggConfig.getCustyggPassword());
    DefaultHttpClient client = new DefaultHttpClient();
    client.getCredentialsProvider().setCredentials(
        new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT),
        new UsernamePasswordCredentials(custyggConfig.getCustyggUser(), passwordDecrypted));
    client.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);

    return client;

  }

  private static String decryptPassword(String password) {
    try {
      return FormatUtilities.encryptDecrypt(password, false);
    } catch (ServletException e) {
      throw new OBException(OBMessageUtils.messageBD("CUSTYGG_ErrorEncryptingPass"));
    }
  }

  public static CustyggWebserviceConfig getCUSTYGGConfig() {
    // Retrieve configuration
    OBCriteria<CustyggWebserviceConfig> custyggConfigCriteria = OBDal.getInstance().createCriteria(
        CustyggWebserviceConfig.class);

    custyggConfigCriteria.add(Restrictions.eq(CustyggWebserviceConfig.PROPERTY_ACTIVE, true));
    custyggConfigCriteria.setMaxResults(1);

    CustyggWebserviceConfig custyggConfig = (CustyggWebserviceConfig) custyggConfigCriteria
        .uniqueResult();
    if (custyggConfig == null) {
      throw new OBException(OBMessageUtils.messageBD("CUSTYGG_ConfigurationError"));

    }
    return custyggConfig;
  }

  public static JSONObject prepareCheckaGiftCardPOSTJsonInfo(String code) {
    JSONObject json = new JSONObject();
    try {
      json.put("code", code);
    } catch (JSONException ignore) {
    }
    return json;
  }

  public static JSONObject prepareRedeemaGiftCardPOSTPetition(String code,
      String store_staff_position, String store_staff_name, String store_location) {
    JSONObject json = new JSONObject();
    try {
      json.put("code", code);
      json.put("store_staff_position", store_staff_position);
      json.put("store_staff_name", store_staff_name);
      json.put("store_location", store_location);
    } catch (JSONException ignore) {
    }
    return json;
  }
}
