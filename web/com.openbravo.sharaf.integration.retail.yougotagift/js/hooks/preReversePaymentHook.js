/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global _, CUSTYGG*/

OB.UTIL.HookManager.registerHook('OBPOS_preReversePayment', function (args, callbacks) {
  var kind = args.paymentToReverse.get('kind');
  if (OB.MobileApp.model.paymentnames[kind].paymentMethod.paymentProvider === 'CUSTYGG_.UI.YouGotAGift') {
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTARM_ReversePayment'), OB.I18N.getLabel('CUSTYGG_ReversePaymentImpossible'), [{
      label: OB.I18N.getLabel('OBMOBC_LblOk'),
      isConfirmButton: true,
      action: function () {
        args.cancellation = true;
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }]);
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
});