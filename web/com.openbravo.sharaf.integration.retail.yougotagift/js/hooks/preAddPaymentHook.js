/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global _, CUSTYGG*/

OB.UTIL.HookManager.registerHook('OBPOS_preAddPayment', function (args, callbacks) {
  var kind = args.paymentToAdd.get('kind');
  var payments = OB.MobileApp.model.receipt.get('payments');
  if (args.receipt.getGross() <= 0 && OB.MobileApp.model.paymentnames[kind].paymentMethod.paymentProvider === 'CUSTYGG_.UI.YouGotAGift') {
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), OB.I18N.getLabel('CUSTYGG_ReturnPaymentImpossible'), [{
      label: OB.I18N.getLabel('OBMOBC_LblOk'),
      isConfirmButton: true,
      action: function () {
        args.cancellation = true;
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }]);
  } else if (OB.MobileApp.model.paymentnames[kind].paymentMethod.paymentProvider === 'CUSTYGG_.UI.YouGotAGift') {
    var yggUsed = _.some(payments.models, function (ygg) {
      return OB.MobileApp.model.paymentnames[ygg.get('kind')].paymentMethod.paymentProvider === 'CUSTYGG_.UI.YouGotAGift' && ygg.get('paymentData') && args.paymentToAdd.get('paymentData') && ygg.get('paymentData').yggCardNumber === args.paymentToAdd.get('paymentData').yggCardNumber;
    });
    if (yggUsed) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), OB.I18N.getLabel('CUSTYGG_YGGUsed'), [{
        label: OB.I18N.getLabel('OBMOBC_LblOk'),
        isConfirmButton: true,
        action: function () {
          args.cancellation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      }]);
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
});