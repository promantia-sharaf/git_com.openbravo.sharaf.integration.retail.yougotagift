/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global _, CUSTYGG*/

OB = OB || {};
OB.UTIL = OB.UTIL || {};
OB.UTIL.yggcardPreOrderSave = function (args, callbackSuccess, callbackError) {
  var receipt = args.receipt,
      resultMsg = [],
      error = false,
      paymentsCheked = null,
      returnResult = null,
      paymentData = null;

  //Redemption Cards
  var yggCardPayments = _.filter(receipt.get('payments').models, function (payment) {
    return OB.MobileApp.model.paymentnames[payment.get('kind')].paymentMethod.paymentProvider === 'CUSTYGG_.UI.YouGotAGift';
  });
  returnResult = function () {
    if (error) {
      callbackError(resultMsg);
    } else {
      callbackSuccess();
    }
  };
  paymentsCheked = _.after(yggCardPayments.length, returnResult);

  // Process Redemption Giftcard Payment
  _.each(yggCardPayments, function (payment) {
    if (OB.UTIL.isNullOrUndefined(payment.get('yggRedeemed'))) {
      CUSTYGG.UTIL.callRedeemaGiftCard({
        code: payment.get('paymentData').yggCardNumber,
        store_staff_position: OB.MobileApp.model.usermodel.get('name'),
        store_staff_name: OB.MobileApp.model.get('terminal').client$_identifier,
        store_location: OB.MobileApp.model.get('terminalName')
      }, function (data) {
        OB.UTIL.showLoading(false);
        if (!OB.UTIL.isNullOrUndefined(data.code) && OB.UTIL.isNullOrUndefined(data.errors) && OB.UTIL.isNullOrUndefined(data.myError)) {
          payment.set('custyggCode', data.code);
          payment.get('paymentData').custyggCode = data.code;
          payment.set('custyggBrand', data.brand);
          payment.get('paymentData').custyggBrand = data.brand;
          payment.set('custyggStaff', data.staff);
          payment.get('paymentData').custyggStaff = data.staff;
          payment.set('custyggStore', data.store);
          payment.get('paymentData').custyggStore = data.store;
          payment.set('custyggDateRedeemed', data.date_redeemed);
          payment.get('paymentData').custyggDateRedeemed = data.date_redeemed;
          payment.set('custyggRedemptionCode', data.redemption_code);
          payment.get('paymentData').custyggRedemptionCode = data.redemption_code;
          payment.set('yggRedeemed', true);
          error = false;
          paymentsCheked();
        } else {
          var msg;
          if (data.errors) {
            msg = data.errors[0].errors[0];
          } else if (data.myError) {
            msg = data.myError.split(':')[1];
          } else {
            msg = OB.I18N.getLabel('CUSTYGG_UnknowError');
          }
          OB.UTIL.showLoading(false);
          resultMsg.push(msg);
          error = true;
          paymentsCheked();
        }
      }, function (data) {
        var msg;
        if (data.errors) {
          msg = data.errors[0].errors[0];
        } else if (data.myError) {
          msg = data.myError.split(':')[1];
        } else {
          msg = OB.I18N.getLabel('CUSTYGG_ErrorTimeOut');
        }
        OB.UTIL.showLoading(false);
        resultMsg.push(msg);
        error = true;
        paymentsCheked();
      });
    } else {
      error = false;
      paymentsCheked();
    }
  });
};
OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function (args, callbacks) {
  if (args.cancellation) {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    return;
  }

  OB.UTIL.yggcardPreOrderSave(args, function () {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }, function (errorCardNumberList) {
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), errorCardNumberList.join(', '));
    args.cancellation = true;
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
});