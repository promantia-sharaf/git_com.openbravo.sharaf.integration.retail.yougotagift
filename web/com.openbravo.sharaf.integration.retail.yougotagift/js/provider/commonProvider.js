/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, _, $, CUSTYGG */

enyo.kind({
  name: 'CUSTYGG_.UI.YouGotAGift',
  components: [{
    kind: 'Scroller',
    maxHeight: '245px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  }, {
    kind: 'balanceInfo'
  }, {
    style: 'padding: 10px 0px;',
    components: [{
      kind: 'CUSTYGG.UI.ProviderOK'
    }, {
      kind: 'OB.UI.CancelDialogButton'
    }]
  }],
  newAttributes: [{
    kind: 'CUSTYGG.UI.modularRenderTextProperty',
    modelProperty: 'yggCardNumber',
    i18nLabel: 'CUSTYGG_YGGCard',
    maxLength: 39,
    readOnly: false
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.mainPopup.$.header.setContent(OB.I18N.getLabel('CUSTYGG_YGGPaymentHeader'));
    this.$.balanceInfo.hide();
    this.cardDetails = {};
    this.propertycomponents = {};

    enyo.forEach(this.newAttributes, function (field) {
      var editline = this.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + field.name,
        newAttribute: field
      });
      this.propertycomponents[field.name] = editline.propertycomponent;
      this.propertycomponents[field.name].propertiesDialog = this;
    }, this);
  }
});
enyo.kind({
  name: 'balanceInfo',
  style: 'text-align: center;height: 30px;margin-left:40%;margin-bottom:10px;',
  components: [{
    name: 'amount',
    style: 'color:white;font-size: 20px;float:left;'
  }, {
    name: 'totalAmount',
    style: 'color:white;font-size: 20px;margin-right:1%;float:left;'
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.$.amount.setContent(OB.I18N.getLabel('CUSTYGG_LblYGGAmount'));
  }
});
enyo.kind({
  name: 'CUSTYGG.UI.ProviderOK',
  kind: 'OB.UI.ModalDialogButton',
  isDefaultAction: true,
  events: {
    onShowPopup: '',
    onHideThisPopup: ''
  },
  tap: function () {
    OB.UTIL.showLoading(true);
    var me = this,
        cardDetails = {},
        payment, amount, pending;
    this.owner.owner.waterfallDown('onRecoverValues', {
      values: cardDetails
    });
    if (me.owner.$.balanceInfo.$.totalAmount.content === "") {
      CUSTYGG.UTIL.callCheckaGiftCard({
        yggCardNumber: cardDetails.yggCardNumber
      }, function (data) {
        if (!OB.UTIL.isNullOrUndefined(data.code) && OB.UTIL.isNullOrUndefined(data.errors) && OB.UTIL.isNullOrUndefined(data.myError) && OB.UTIL.isNullOrUndefined(data.exception)) {
          OB.UTIL.showLoading(false);
          me.owner.$.balanceInfo.show();
          me.owner.$.balanceInfo.$.totalAmount.setContent(data.amount);
        } else {
          var msg;
          if (data.errors) {
            msg = data.errors[0].errors[0];
          } else if (data.myError) {
            msg = data.myError.split(':')[1];
          } else if (data.exception) {
            msg = data.exception.message;
          } else {
            msg = OB.I18N.getLabel('CUSTYGG_UnknowError');
          }
          OB.UTIL.showLoading(false);
          me.doHideThisPopup();
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), msg);
        }
      }, function (data) {
        var msg;
        if (data.errors) {
          msg = data.errors[0].errors[0];
        } else if (data.myError) {
          msg = data.myError.split(':')[1];
        } else if (data.exception) {
          msg = data.exception.message;
        } else {
          msg = OB.I18N.getLabel('CUSTYGG_UnknowError');
        }
        OB.UTIL.showLoading(false);
        me.doHideThisPopup();
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), msg);
      });
    } else {
      OB.UTIL.showLoading(false);
      payment = OB.MobileApp.model.paymentnames[me.owner.owner.owner.args.key];
      amount = me.owner.$.balanceInfo.$.totalAmount.content;
      pending = me.owner.owner.owner.args.receipt.getPending();
      OB.MobileApp.model.receipt.addPayment(new OB.Model.PaymentLine({
        kind: payment.payment.searchKey,
        name: payment.payment[OB.Constants.IDENTIFIER],
        paymentMethod: me.owner.owner.owner.args.paymentMethod,
        amount: (pending >= amount) ? amount : pending,
        rate: payment.rate,
        mulrate: payment.mulrate,
        isocode: payment.isocode,
        isCash: payment.paymentMethod.iscash,
        allowOpenDrawer: payment.paymentMethod.allowopendrawer,
        openDrawer: payment.paymentMethod.openDrawer,
        printtwice: payment.paymentMethod.printtwice,
        custyggCardAmount: amount,
        paymentData: {
          yggCardNumber: cardDetails.yggCardNumber,
          voidTransaction: this.voidTransaction
        }
      }));
      me.doHideThisPopup();
    }
  },
  voidTransaction: function (callback, receipt, removedPayment) {
    CUSTYGG.UTIL.voidTransaction(callback, receipt, removedPayment);
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblOk'));
  }
});
enyo.kind({
  name: 'CUSTYGG.UI.modularRenderTextProperty',
  kind: 'OB.UI.renderTextProperty',
  handlers: {
    onRecoverValues: 'recoverValues',
    onResetValue: 'resetValue'
  },
  initComponents: function () {
    this.inherited(arguments);
  },
  loadValue: function (inSender, inEvent) {
    if (!OB.UTIL.isNullOrUndefined(inEvent[this.modelProperty])) {
      this.setValue(inEvent[this.modelProperty]);
    }
  },
  resetValue: function (inSender, inEvent) {
    this.setValue('');
  },
  recoverValues: function (inSender, inEvent) {
    inEvent.values[this.modelProperty] = this.getValue();
  }
});