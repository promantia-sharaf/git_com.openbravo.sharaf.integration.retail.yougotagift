/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global, CUSTYGG */

CUSTYGG.UTIL = {

  callCheckaGiftCard: function (cardDetails, callback, callbackError) {
    var process = new OB.DS.Request('com.openbravo.sharaf.integration.retail.yougotagift.process.CallCheckaGiftCard');
    this.genericCall(process, cardDetails, callback, callbackError);
  },
  callRedeemaGiftCard: function (cardDetails, callback, callbackError) {
    var process = new OB.DS.Process('com.openbravo.sharaf.integration.retail.yougotagift.process.CallGiftCardRedemption');
    this.genericCall(process, cardDetails, callback, callbackError);
  },
  genericCall: function (process, cardDetails, callback, callbackError) {
    process.exec(cardDetails, function (data) {
      if (!data || data.exception) {
        if (callbackError && callbackError instanceof Function) {
          callbackError(data);
        }
        return;
      }
      if (callback && callback instanceof Function) {
        callback(data);
      }
    }, function (data) {
      if (callbackError && callbackError instanceof Function) {
        callbackError(data);
      }
    }, true, 40000);
  },
  voidTransaction: function (callback, receipt, removedPayment) {
    if (!OB.UTIL.isNullOrUndefined(removedPayment.get('paymentData').yggRedeemed)) {
      callback(true, OB.I18N.getLabel('CUSTYGG_ErrorRemovePayment'));
      return;
    } else {
      callback(false);
    }
  }
};