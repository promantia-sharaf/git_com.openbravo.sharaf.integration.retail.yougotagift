/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, CUSTYGG */
enyo.kind({
  name: 'OB.UI.MenuCheckYouGotaGift',
  kind: 'OB.UI.MenuAction',
  permission: 'CUSTYGG_YouGotaGift.check',
  events: {
    onShowPopup: '',
    onShowDivText: '',
    onRearrangeEditButtonBar: ''
  },
  i18nLabel: 'CUSTYGG_LblCheckYGG',
  tap: function () {
    if (this.disabled) {
      return true;
    }
    this.inherited(arguments);
    this.doShowPopup({
      popup: 'CUSTYYG.UI.ScanGiftCard'
    });
  },
  init: function (model) {
    this.model = model;
    if (OB.MobileApp.model.hasPermission(this.permission, true)) {
      this.show();
    } else {
      this.hide();
    }
  }
});

enyo.kind({
  kind: 'OB.UI.ModalAction',
  name: 'CUSTYYG.UI.ScanGiftCard',
  i18nHeader: 'CUSTYGG_CheckYGGHeader',
  topPosition: '60px',
  events: {
    onHideThisPopup: ''
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '375px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.ModalDialogButton',
      name: 'apply',
      isDefaultAction: true,
      i18nContent: 'CUSTYGG_LblApplyButton',
      tap: function () {
        this.owner.owner.actionApply();
      }
    }, {
      kind: 'OB.UI.ModalDialogButton',
      name: 'cancel',
      isDefaultAction: false,
      i18nContent: 'CUSTYGG_LblCancelButton',
      tap: function () {
        this.owner.owner.actionCancel();
      }
    }]
  },
  newAttributes: [{
    kind: 'CUSTYGG.UI.modularRenderTextProperty',
    name: 'youGotAGiftNumber',
    modelProperty: 'yggCardNumber',
    i18nLabel: 'CUSTYGG_YGGCard',
    mandatory: true
  }],
  actionApply: function () {
    OB.UTIL.showLoading(true);
    var me = this,
        cardDetails = {};
    this.owner.owner.waterfallDown('onRecoverValues', {
      values: cardDetails
    });
    CUSTYGG.UTIL.callCheckaGiftCard({
      yggCardNumber: cardDetails.yggCardNumber
    }, function (data) {
      if (!OB.UTIL.isNullOrUndefined(data.code) && OB.UTIL.isNullOrUndefined(data.errors) && OB.UTIL.isNullOrUndefined(data.myError) && OB.UTIL.isNullOrUndefined(data.exception)) {
        var info = [{
          content: ''
        }, {
          allowHtml: true,
          content: '&nbsp;'
        }];
        info.push({
          content: OB.I18N.getLabel('CUSTYGG_Code') + data.code + '.\n'
        });
        info.push({
          content: OB.I18N.getLabel('CUSTYGG_Brand') + data.brand + '.\n'
        });
        info.push({
          content: OB.I18N.getLabel('CUSTYGG_DatePurchased') + data.date_purchased + '.\n'
        });
        info.push({
          content: OB.I18N.getLabel('CUSTYGG_ExpiryDate') + data.expiry_date + '.\n'
        });
        info.push({
          content: OB.I18N.getLabel('CUSTYGG_Amount') + data.amount + '.\n'
        });
        info.push({
          content: OB.I18N.getLabel('CUSTYGG_AmountInUSD') + data.amount_in_usd + '.\n'
        });

        me.propertycomponents.youGotAGiftNumber.setValue('');
        me.hide();
        OB.UTIL.showLoading(false);
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGHeaderInfo'), info);
      } else {
        var msg;
        if (data.errors) {
          msg = data.errors[0].errors[0];
        } else if (data.myError) {
          msg = data.myError.split(':')[1];
        } else if (data.exception) {
          msg = data.exception.message;
        } else {
          msg = OB.I18N.getLabel('CUSTYGG_UnknowError');
        }
        OB.UTIL.showLoading(false);
        me.doHideThisPopup();
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), msg, [{
          isConfirmButton: true,
          label: OB.I18N.getLabel('OBMOBC_LblOk'),
          previous: me,
          action: function (previous) {
            previous.owner.owner.hide();
            this.previous.show();
          }
        }]);
      }
    }, function (data) {
      var msg;
      if (data.errors) {
        msg = data.errors[0].errors[0];
      } else if (data.myError) {
        msg = data.myError.split(':')[1];
      } else if (data.exception) {
        msg = data.exception.message;
      } else {
        msg = OB.I18N.getLabel('CUSTYGG_UnknowError');
      }
      OB.UTIL.showLoading(false);
      me.doHideThisPopup();
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTYGG_YGGErrorHeader'), msg, [{
        isConfirmButton: true,
        label: OB.I18N.getLabel('OBMOBC_LblOk'),
        previous: me,
        action: function (previous) {
          previous.owner.owner.hide();
          this.previous.show();
        }
      }]);
    });
  },
  actionCancel: function () {
    this.propertycomponents.youGotAGiftNumber.setValue('');
    this.hide();
  },
  executeOnShow: function () {
    var me = this;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.propertycomponents = {};

    enyo.forEach(this.newAttributes, function (field) {
      var editline = this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + field.name,
        newAttribute: field
      });
      this.propertycomponents[field.name] = editline.propertycomponent;
      this.propertycomponents[field.name].propertiesDialog = this;
    }, this);
  }
});
enyo.kind({
  name: 'CUSTYGG.UI.modularRenderTextProperty',
  kind: 'OB.UI.renderTextProperty',
  handlers: {
    onRecoverValues: 'recoverValues',
    onResetValue: 'resetValue'
  },
  initComponents: function () {
    this.inherited(arguments);
  },
  loadValue: function (inSender, inEvent) {
    if (!OB.UTIL.isNullOrUndefined(inEvent[this.modelProperty])) {
      this.setValue(inEvent[this.modelProperty]);
    }
  },
  resetValue: function (inSender, inEvent) {
    this.setValue('');
  },
  recoverValues: function (inSender, inEvent) {
    inEvent.values[this.modelProperty] = this.getValue();
  }
});
OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTYYG.UI.ScanGiftCard',
  name: 'CUSTYYG.UI.ScanGiftCard'
});
OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
  kind: 'OB.UI.MenuCheckYouGotaGift'
});